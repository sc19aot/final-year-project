# final-year-project

Repository for COMP3931 Individual Project - PABANA (Personal Assistant Bot Assisting Neurodiverse Academics)

## About this project

This repository contains the source code for a chatbot developed by Ashley Osei-Thompson. The chatbot is an assistive AI tool which aids neurodiverse university academics at the University of Leeds to navigate the Minerva digital system. 

The chatbot's primary goal is to provide support in five different areas of Minerva usage: staff admin, accessibility of teaching materials, student engagement, content creation and student assessments.



