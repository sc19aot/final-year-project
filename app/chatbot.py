import os
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

# Setting the path of the database
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

#Setting up the chatbot so that it can connect to database
chatbot = ChatBot("P.A.B.A.N.A", storage_adapter='chatterbot.storage.SQLStorageAdapter',
logic_adapters=[
    {
            'import_path': 'chatterbot.logic.BestMatch',
            'default_response': 'I am sorry, I am not quite sure I understand.',
            'maximum_similarity_threshold': 0.90
    }
    ],
    database_uri=SQLALCHEMY_DATABASE_URI)

#Training the chatbot with custom db so that it learns to communicate with the user
trainer = ChatterBotCorpusTrainer(chatbot)

# Training the chatbot with custom corpus
trainer.train('chatterbot.corpus.custom',
    "chatterbot.corpus.english.conversations")
