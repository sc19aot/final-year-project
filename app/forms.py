from flask_wtf import Form
from wtforms.validators import DataRequired
from wtforms import TextField
from wtforms import DateField

class ExampleForm(Form):
    taskname = TextField('taskname', validators=[DataRequired()])
    