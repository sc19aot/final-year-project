# Unit tests for ChatBot
from unittest import TestCase
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

# Creating an instance of the chatbot to use for testing purposes
def create_Chatbot(self):
    testbot = ChatBot("P.A.B.A.N.A", storage_adapter='chatterbot.storage.SQLStorageAdapter',
    logic_adapters=[
        {
                'import_path': 'chatterbot.logic.BestMatch',
                'default_response': 'I am sorry, I am not quite sure I understand.',
                'maximum_similarity_threshold': 0.90
        }
        ])
    trainer = ChatterBotCorpusTrainer(testbot)

    return testbot

# A class for unit testing of the chatbot
class ChatbotUnitTests(TestCase):

    # Test 1: Checking that the chatbot provides a greeting to the user
    def test_chatbot_greeting(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("hello")
        if "Hi" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 2: Checking that the chatbot introduces itself when asked by the user
    def test_chatbot_introduction(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("Who are you?")
        if "I'm P.A.B.A.N.A" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 3: Checking that the chatbot can be used by multiple users concurrently
    def test_chatbot_concurrency(self):
        testbot1 = create_Chatbot(self)
        testbot2 = create_Chatbot(self)
        bot_msg1 = testbot1.get_response("hi")
        bot_msg2 = testbot2.get_response("hi")
        if "Hello" in str(bot_msg1):
            x = True
        else:
            x = False
        if "Hello" in str(bot_msg2):
            y = True
        else:
            y = False
        z = x and y
        self.assertTrue(z)

    # Test 4: Checking that the chatbot informs the user if it cannot answer a question
    def test_random_question(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("who is the prime minister?")
        if "I am not quite sure I understand" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 5: Checking that the chatbot provides an answer to a Minerva accessibility related question
    def test_chatbot_minerva_accessibility(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how can I improve image quality?")
        if "Here is a link on how to create high resolution images for users with low vision: https://www.perkinselearning.org/technology/blog/how-create-high-resolution-images-users-low-vision-0" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 6: Checking that the chatbot provides an answer to the same Minerva accessibility related question
    #  as Test 5 using different words
    def test_chatbot_minerva_accessibility_variation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I improve image quality?")
        if "Here is a link on how to create high resolution images for users with low vision: https://www.perkinselearning.org/technology/blog/how-create-high-resolution-images-users-low-vision-0" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 7: Checking that the chatbot provides an answer to a Minerva admin related question
    def test_chatbot_minerva_admin(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how can I edit my staff profile?")
        if "Jadu" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 8: Checking that the chatbot provides an answer to the same Minerva admin related question
    #  as Test 7 using different words
    def test_chatbot_minerva_admin_variation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I change my staff profile?")
        if "Jadu" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 9: Checking that the chatbot provides an answer to a Minerva content creation related question
    def test_chatbot_minerva_content_creation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how can I add content to my module?")
        if "This can be done by clicking the module menu and navigating to the area you would like to add content to" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 10: Checking that the chatbot provides an answer to the same Minerva content creation related question
    #  as Test 9 using different words
    def test_chatbot_minerva_content_creation_variation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I add content to a module?")
        if "This can be done by clicking the module menu and navigating to the area you would like to add content to" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 11: Checking that the chatbot provides an answer to a Minerva assessment related question
    def test_chatbot_minerva_assessments(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I change the question score for a test?")
        if "In the Test Canvas page you can review the question settings, press the Question Settings button on the right" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 12: Checking that the chatbot provides an answer to the same Minerva assessment related question
    #  as Test 11 using different words
    def test_chatbot_minerva_assessments_variation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I alter the question score for a test?")
        if "In the Test Canvas page you can review the question settings, press the Question Settings button on the right" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 13: Checking that the chatbot provides an answer to a Minerva engagement related question
    def test_chatbot_minerva_engagement(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how do I create a discussion board?")
        if "Navigate to the content area of your module where you would like to add a discussion board" in str(bot_msg):
            x = True
        self.assertTrue(x)

    # Test 14: Checking that the chatbot provides an answer to the same Minerva engagement related question
    #  as Test 13 using different words
    def test_chatbot_minerva_engagement_variation(self):
        testbot = create_Chatbot(self)
        bot_msg = testbot.get_response("how can I make a discussion board?")
        if "Navigate to the content area of your module where you would like to add a discussion board" in str(bot_msg):
            x = True
        self.assertTrue(x)
