# Information from the Digital Practice Leeds site has been used to produce answers to stated questions
# Can be found at: https://digitalpractice.leeds.ac.uk/accessibility-inclusivity/
categories:
- accessibility
conversations:
- - how can I check the accessibility of my teaching resources?
  - 'Blackboard Ally analyses most commonly used content (documents, images, presentations etc) within each Minerva module, identifying any issues regarding accessibility as well as providing detailed instructions for how to fix them. 
    Each item is given a percentage score as well as a red/amber/green rating, and each module has an accessibility report available under ‘module tools’, which includes an overall score as well as lists of the most common issues and all items by accessibility score.
    Here is a link about how to use Blackboard Ally: https://www.leeds.ac.uk/vle/staff/build/ally/' 
- - how do I make my video content accessible?
  - 'From 23rd September 2020, all videos which are uploaded to Minerva or any other online space used for teaching 
    – e.g. Teams, OneDrive – need to include captions and an audio description of any purely visual content (this description can be included in the captions, however, it doesn’t need to be separate).
    There are two university systems that will automatically create captions for video content – Mediasite and Stream. 
    Mediasite is the tool which is used for lecture capture, but can also create desktop capture videos and video files can be uploaded directly to it. 
    Stream allows you to upload video files directly, and recorded meetings from Teams are located in the Stream of the person who started the recording. 
    Some versions of PowerPoint allow presentations to be published directly to Stream as a video.'
- - how do I make my Microsoft Word/Excel/Powerpoint/PDF documents accessible?
  - "Here are the general principles of good practice for optimum readability:
    - Sans serif font (e.g. Arial, Calibri, Verdana) in all cases.
    - Minimum text size 12 for Word documents, emails, and similar formats designed to be read on a screen.
    - Minimum text size 24 for PowerPoint and other presentations designed to be displayed using a projector or in a webinar. This optimises the accessibility for most formats, including viewing on a mobile device. The font may need to be bigger for some screens/ venues and 32 should be a minimum for widescreen format presentations., but the font may need to be bigger for display in large rooms with small display screens.
    - Use a pastel coloured background for all documents and emails, and avoid using colour alone to convey meaning.
    - Documents should use Headings (Word), or slide Titles (PowerPoint).
    - The general aim for all sentences and paragraphs should be that they are concise (around 25 words or five lines respectively) but defer to your own subject knowledge and pedagogy.
    - When writing for emphasis, avoid underlining, italics or all capitals; use bold instead. Italics may still be used in academic conventions such as book titles.
    - Avoid underlining, italics, or all capitals for emphasis; use bold instead.
    - Try to avoid multi-clause sentences (one that is made up of a main clause and then one or more dependent clauses) where possible but defer to your own subject knowledge and pedagogy.
    - Links should be meaningful text which states what the link is to, rather than e.g. “click here” or “link”.
    - Avoid images of text, unless there is a pedagogic need to share it as an image.
    - Use high-quality images so that they don’t appear pixilated when magnified.
    - Accessible templates are provided.
    - Here is a link to the Microsoft accessibility checker if you need more information: https://support.microsoft.com/en-us/office/improve-accessibility-with-the-accessibility-checker-a16f6de0-2f39-4a2b-8bd8-5ad801426c7f?ui=en-us&rs=en-us&ad=us#PickTab=Windows"
- - how do I create accessible links?
  - "There are three main things:
    1. The link on the webpage should open in the same web browser window unless its necessary to have two windows/tabs open at the same time.
    Opening links in a new window removes the element of choice for the user, they should be allowed to control their own experience.
    2. Make sure the link has meaningful text so that the person navigating your content knows what they're clicking.
    3. Be upfront about the specific content a link will launch e.g audio, PDF, video, external website"
- - how can I make a file name easy to read?
  - 'You should stick to file naming conventions. Choose a meaningful filename and use CamelCase (capitalising the first letter of each word in the filename) to make it easier for everyone to read.'
- - how can I convert my handwritten notes to text?
  - 'This can be achieved in different ways, for example automatically using Optical Character Recognition (OCR) software, manually typed, or by audio-narrating the content and auto-captioning the resulting recording. 
    Microsoft Office Lens is available for Android and IoS devices (Apple) and can scan handwritten text and convert it to OCR.
    - Here is a link for Android: https://support.microsoft.com/en-us/office/microsoft-lens-for-android-ec124207-0049-4201-afaf-b5874a8e6f2b
    - And a link if you have an Apple device: https://support.microsoft.com/en-us/office/microsoft-lens-for-ios-fbdca5f4-1b1b-4391-a931-dc1c2582397b#:~:text=Microsoft%20Word%20Office%20Lens%20can,you%20authored%20the%20document%20yourself.'
- - how can I improve image quality?
  - 'Here is a link on how to create high resolution images for users with low vision: https://www.perkinselearning.org/technology/blog/how-create-high-resolution-images-users-low-vision-0'


