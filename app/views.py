from .chatbot import *
from flask import render_template, request, url_for, redirect
from app import app
from app import db, models
import datetime
from .forms import *

# Route for main web page containing the chatbot
@app.route('/', methods=['GET'])
def index(): #shows the homepage
    return render_template("index.html")

# Retrieving the chatbot's responses from the user input
@app.route('/getChatbotResponse')
def getBotResponse():
    msgInput = request.args.get('message')
    msgOutput = str(chatbot.get_response(msgInput))
    return msgOutput
